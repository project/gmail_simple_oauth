<?php

namespace Drupal\gmail_simple_oauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Require Login settings for this site.
 */
class GmailSimpleOAuthSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gmail_simple_oauth_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gmail_simple_oauth.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gmail_simple_oauth.config');

    // Login and destination paths.
    $form['google_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Client ID'),
      '#description' => $this->t('Google Client ID'),
      '#default_value' => $config->get('google_client_id') ? $config->get('google_client_id') : '',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $absolutes = [];

    if(empty($form_state->getValue('google_client_id'))){
      $form_state->setErrorByName('google_client_id', $this->t('Complete the Google Client ID field'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('gmail_simple_oauth.config')
      ->set('google_client_id', $form_state->getValue('google_client_id'))
      ->save();

    // Must rebuild caches for settings to take immediate effect.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
